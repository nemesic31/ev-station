import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Button } from 'react-native'
import { LoginButton, AccessToken } from 'react-native-fbsdk';

export default class SignInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fbData: null
    };
  }
  render() {
    const { navigate } = this.props.navigation;
    const { fbData } = this.state
    console.log('hello');
    // if (!fbData) {
    //   window.location.replace('main');
    // } else {
    return (
      <View style={{ backgroundColor: 'rgb(255,126,73)', width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
        <TouchableOpacity
          style={{ backgroundColor: '#3b5998', padding: 10, borderRadius: 10 }}
          onPress={() => navigate('Main')}
        >
          <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}> Sign In with Facebook </Text>
        </TouchableOpacity>
        <LoginButton
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    console.log(data.accessToken.toString())
                    this.setState({ fbData: data })
                    navigate('Main')
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")} />
      </View>
    )
    // }

  }
}
