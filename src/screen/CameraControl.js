import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Alert, Dimensions } from 'react-native';
import MapView, { ProviderPropType } from 'react-native-maps';
import axios from 'react-native-axios';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 13.744360;
const LONGITUDE = 100.543503;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class CameraControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLongtitude: null,
      showLatitude: null,
      showLatitudeDelta: LATITUDE_DELTA,
      showLongtitudeDelta: LONGITUDE_DELTA,
      allData: [],
      boundingBox: {},
      address: null,
      mapRegion: null,
      latitude: null,
      longitude: null,
    };
  }

  getData = () => {
    console.log('hello');
    const { latitude, longitude } = this.state
    const data = { latLng: { lat: latitude, lng: longitude }, distance: 5 }
    if (latitude && longitude) {
      axios
        .post('https://developers.mea.or.th/api/ev/station/distance', data,
          {
            headers: {
              'Content-Type': 'application/json'
            },
          }
        )
        .then((response) => {
          const dataRes = response.data;
          this.setState({ allData: dataRes });
          console.log(this.state.allData);
        })
        .catch((error) => {
          this.setState({ error: 'Something just went wrong' });
        });
    }
  };

  handleZoom = () => {
    const { boundingBox } = this.state
    const data = boundingBox;
    if (boundingBox) {
      axios
        .post('https://developers.mea.or.th/api/ev/station/map-coordinate', data,
          {
            headers: {
              'Content-Type': 'application/json'
            },
          }
        )
        .then((response) => {
          if (response.data) {
            const dataRes = response.data;
            this.setState({ allData: dataRes });
            console.log(this.state.allData);
          }
        })
        .catch((error) => {
          this.setState({ error: 'Something just went wrong' });
        });
    }
  };

  async componentDidMount() {
    this.getCurrentPosition();
    this.getData()
  }

  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }


  getDelta = async () => {
    const delta = await this.map.__lastRegion;
    this.setState({ showLatitudeDelta: delta.latitudeDelta })
    this.setState({ showLongtitudeDelta: delta.longitudeDelta })
    this.setState({ showLatitude: delta.latitude });
    this.setState({ showLongtitude: delta.longitude });
    this.getBoundingBox()
  }
  getBoundingBox = async () => {
    const { showLatitude, showLongtitude, showLatitudeDelta, showLongtitudeDelta, latitudeDelta } = this.state
    const newBoundingBox = {
      minLongitude: showLongtitude - showLongtitudeDelta / 2, // westLng - min lng
      minLatitude: showLatitude - showLatitudeDelta / 2, // southLat - min lat
      maxLongitude: showLongtitude + showLongtitudeDelta / 2, // eastLng - max lng
      maxLatitude: showLatitude + showLatitudeDelta / 2 // northLat - max lat
    }
    this.setState({ boundingBox: newBoundingBox })

    this.handleZoom()

  }

  getCurrentPosition = () => {
    this.watchID = Geolocation.getCurrentPosition(
      async (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };
        Geocoder.from(position.coords.latitude, position.coords.longitude)
          .then((json) => {
            this.setState({ address: json.results[2].formatted_address });
          })
          .catch((error) => console.log(error));
        this.getData()
        await this.onRegionChange(region, region.latitude, region.longitude);
      },
      (error) => console.log(error),
      { enableHighAccuracy: true, timeout: 200000 }
    );
  };

  onRegionChange = (region, lastLat, lastLong) => {
    this.setState({
      mapRegion: region,
      latitude: lastLat || this.state.latitude,
      longitude: lastLong || this.state.longitude,
    });
  };




  render() {
    const { showLatitude, showLongtitude, allData, latitude, longitude, mapRegion, address } = this.state;
    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          ref={ref => {
            this.map = ref;
          }}
          style={styles.map}
          initialRegion={mapRegion}
          onRegionChangeComplete={() => this.getDelta()}
          onMapReady={() => this.getData()}
          maxDelta={0.5}
        >
          {allData ? allData.map(element =>
            <MapView.Marker
              title={element.name}
              coordinate={{
                latitude: element.latitude,
                longitude: element.longitude,
              }}
            />
          ) : this.getData()}
          {longitude && latitude && (
            <MapView.Marker
              title={address}
              coordinate={{
                latitude: latitude, longitude: longitude
              }}
            >
              <View style={styles.radius}>
                <View style={styles.marker} />
              </View>
            </MapView.Marker>
          )}
        </MapView>
        <View style={styles.buttonContainer}>
          <Text style={styles.bubble}>
            latitude:{!showLatitude ? latitude : showLatitude}{'\n'}
            longtitude:{!showLongtitude ? longitude : showLongtitude}
          </Text>
        </View>
      </View>
    );
  }
}

CameraControl.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  buttonContainer: {
    flexDirection: 'column',
    marginVertical: 20,
    backgroundColor: 'transparent',
    marginBottom: 50
  },
  marker: {
    height: 20,
    width: 20,
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 20 / 2,
    overflow: 'hidden',
    backgroundColor: '#007Aff',
  },
  radius: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    overflow: 'hidden',
    backgroundColor: 'rgba(0,122,255,0.1)',
    borderWidth: 1,
    borderColor: 'rgba(0, 112, 255,0.3)',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CameraControl;