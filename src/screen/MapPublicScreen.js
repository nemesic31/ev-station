import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Dimensions,
  PermissionsAndroid,
  Image,
  Text,
  Animated,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';

const styles = StyleSheet.create({
  containerPanel: {
    // height: '100%',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },
  panelHeader: {
    height: 120,
    backgroundColor: '#fbfbfd',
    padding: 24,
  },
  textTitleHeader: {
    fontSize: 20,
    color: '#E07333',
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

Geocoder.init('AIzaSyANMqeZOb82fO-iidbI0if4i9hM_eWvlY8');

class MapPublicScreen extends React.Component {

  constructor(props) {
    super(props);
    this.localNotify = {};
  }

  draggedValue = new Animated.Value(150);

  state = {
    mapRegion: null,
    latitude: null,
    longitude: null,
    markers: [],
    buttonHide: false,
    photo: null,
    address: null,
    showPanel: false,
    icon: 'chevron-small-up',
  };

  async componentDidMount() {
    this.getCurrentPosition();
  }

  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }


  getCurrentPosition = () => {
    this.watchID = Geolocation.getCurrentPosition(
      (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };
        Geocoder.from(position.coords.latitude, position.coords.longitude)
          .then((json) => {
            this.setState({ address: json.results[2].formatted_address });
          })
          .catch((error) => console.log(error));
        this.onRegionChange(region, region.latitude, region.longitude);
      },
      (error) => console.log(error),
      { enableHighAccuracy: true, timeout: 200000 }
    );
  };

  onRegionChange = (region, lastLat, lastLong) => {
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      latitude: lastLat || this.state.latitude,
      longitude: lastLong || this.state.longitude,
    });
    console.log(this.state.lastLat);
  };



  render() {
    const { latitude, longitude, mapRegion, address } = this.state;

    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={mapRegion}
        >
          {longitude && latitude && (
            <MapView.Marker title={address} coordinate={{ latitude: latitude, longitude: longitude }} />
          )}
        </MapView>
      </View>
    );
  }
}

MapPublicScreen.propTypes = {
  provider: ProviderPropType,
};

MapPublicScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MapPublicScreen;
