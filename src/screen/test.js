import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { LoginButton, AccessToken } from 'react-native-fbsdk';

export default class test extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
        <Text> textInComponent </Text>
        <LoginButton
          onLogoutFinished={() => navigate('SignIn')} />
      </View>
    )
  }
}
