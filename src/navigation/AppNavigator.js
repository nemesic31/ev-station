import * as React from 'react';
import 'react-native-gesture-handler';
import { View, Text } from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import CameraControl from '../screen/CameraControl';
import helloTest from '../screen/test';
import SignIn from '../screen/SignInScreen';

const Tab = createBottomTabNavigator();

function MapTab() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Map"
        component={CameraControl}
        options={{
          headerLeft: false,
          headerTitle: 'EV-station',
          headerTitleStyle: {
            color: '#fff'
          },
          headerStyle: {
            backgroundColor: 'rgb(255,126,73)'
          }
        }}
      />
    </Stack.Navigator>
  )
}
function HomeTab() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={helloTest}
        options={{
          headerLeft: false,
          headerTitle: 'Home',
          headerTitleStyle: {
            color: '#fff'
          },
          headerStyle: {
            backgroundColor: 'rgb(255,126,73)'
          }
        }}
      />
    </Stack.Navigator>
  )
}

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Map"
      tabBarOptions={{
        activeTintColor: 'rgb(255,126,73)',
      }}
    >
      <Tab.Screen
        name="Map"
        component={MapTab}
        options={{
          tabBarLabel: 'Map',
          hearderTitle: 'Map'
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeTab}
        options={{
          tabBarLabel: 'Home',
          hearderTitle: 'Home'
        }}
      />
    </Tab.Navigator>
  )
}

const Stack = createStackNavigator();

export default function AppNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="SignIn" component={SignIn} options={{ headerShown: false }} />
        <Stack.Screen name="Main" component={MyTabs} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

