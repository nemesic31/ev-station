import React, { Component } from 'react';
import CameraControl from './src/screen/CameraControl';
import AppNavigator from './src/navigation/AppNavigator';

export default class App extends Component {

  render() {
    
    return <AppNavigator />;
  }
}